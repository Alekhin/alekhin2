import java.util.Scanner;

/**
 * Created by Jenya on 23.03.2017.
 */
public class Calculator {
    public static void main(String[] args) {
        int n1, n2;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введи первое число: ");
        n1 = scan.nextInt();
        System.out.println("Введи второе число: ");
        n2 = scan.nextInt();
        scan.close();
        System.out.println("Результат: " + Sum.Add(n1, n2));
    }
}

